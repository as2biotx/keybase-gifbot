# MR.Bot (Merge Request Bot)

## Introduction

Keybase bot to listen for brought-in merge requests on Gitlab. When a merge request has been brought in, then the bot will post a celebratory GIF.

## Generate GIF

To generate a random GIF, use ```gifScaper.getRandomGif()```.

## Add bot to Keybase chat

1. Install [Keybase](https://keybase.io)
2. Log in to Keybase with the account "mrconfettibot" (i.e. `keybase login mrconfettibot`)
3. Run the python script (i.e. `python3 MrConfettiBot.py`)
4. Install the bot in a channel you like. To do so, click on the little info symbol in your keybase app and select the bot header. Click on "Add a bot" and search for "mrconfettibot". If you want the bot to respond to messages from the gitlab bot, it is not necessary to make the mrconfettibot an unrestricted bot. As a restricted bot, it can already read the messages of other bots.

To log in, the password of the account "mrconfettibot" is required.

## Data source

GIFs carefully curated from [Giphy](https://giphy.com/)
