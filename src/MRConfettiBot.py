#!/usr/bin/env python3

import asyncio
import logging
import os
import sys
import re
import requests
import random

import pykeybasebot.types.chat1 as chat1
from pykeybasebot import Bot

logging.basicConfig(level=logging.DEBUG)

if "win32" in sys.platform:
    asyncio.set_event_loop_policy(
        asyncio.WindowsProactorEventLoopPolicy()  # type: ignore
    )

class GifScraper:
    def __init__(self):
        self.search_terms = ["wohoo", "celebrate", "confetti"]

    @staticmethod
    def extractGifLinks(search_term):
        """
        Extracts all relevant GIFs relating to the search_term

        Input
        -----
        search_term: (String)

        Output
        -----
        gif_links: [String] GIF links related to the search_term
        """

        base_url = f'https://giphy.com/search/{search_term}'

        html_doc = requests.get(base_url).text

        gifs = re.findall(string=html_doc, pattern=r'"mp4": "(.*?)\.mp4')

        unique_gifs = {}

        for i in gifs:
            idx = i.rindex('/')
            if i[:idx] not in unique_gifs.keys():
                unique_gifs[re.sub(string=i[:idx], pattern='media\d', repl='media')] = i[idx + 1:]
            else:
                continue

        gif_links = [k + '/giphy.mp4'.format(unique_gifs[k]) for k in unique_gifs.keys()]

        return gif_links


    def getRandomGif(self):
        """
        Gets random GIF, based on one of the search_terms, which are also randomly selected.

        Input
        -----
        NONE

        Output
        ------
        gif_link: (String) Link for GIF
        """

        # Randomly select one of the search terms
        random.shuffle(self.search_terms)

        # Extract GIF links from page pertaining to search terms
        gif_links = self.extractGifLinks(search_term=self.search_terms[0])

        # Randomly choose one
        random.shuffle(gif_links)
        gif_link = gif_links[0]

        return gif_link

async def handler(bot, event):
    g = GifScraper()
    # Listen to when "merged" in message.
    if event.msg.content.type_name != chat1.MessageTypeStrings.TEXT.value:
        return
    if "merged" in event.msg.content.text.body:
        channel = event.msg.channel
        await bot.chat.send(channel, g.getRandomGif())


listen_options = {}

bot = Bot(username="MRConfettiBot", paperkey=os.getenv("KEYBASE_PAPERKEY"), handler=handler)

asyncio.run(bot.start(listen_options))
